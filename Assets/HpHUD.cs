﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HpHUD : MonoBehaviour {

    public BallShoot Player;
    public float localY;
    public float localX;
    // Update is called once per frame
    void Update()
    {
        gameObject.transform.localScale = new Vector3(localX / 100 * Player.getHP(), localY, 1);
    }
}
