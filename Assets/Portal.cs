﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour {
    public int id;
    public Portal destined;
    public PortalSystem PS;
    public bool disabled;
    public float disabledTime = 2F;
    private void OnTriggerEnter(Collider other)
    {
        if (!disabled&&other.gameObject.tag=="Player")
        {
            PS.UsePortal(this, destined);
        }
        
    }
    public IEnumerator enable()
    {
        disabled = true;
        yield return new WaitForSeconds(disabledTime);
        disabled = false;
    }

}
