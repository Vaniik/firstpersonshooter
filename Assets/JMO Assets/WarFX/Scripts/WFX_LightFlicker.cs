using UnityEngine;
using System.Collections;

/**
 *	Rapidly sets a light on/off.
 *	
 *	(c) 2015, Jean Moreno
**/

[RequireComponent(typeof(Light))]
public class WFX_LightFlicker : MonoBehaviour
{
	public float time = 0.05f;
	
	private float timer;
    public ParticleSystem fire;
    private bool islight=false;

	
	void Update ()
	{
        if (fire.isPlaying&&!islight)
        {
            StartCoroutine(Flicker());
        }
	}
	
	IEnumerator Flicker()
	{
        islight = true;
        GetComponent<Light>().enabled = true;
        yield return new WaitForSeconds(0.2F);
        GetComponent<Light>().enabled = false;
        islight = false;

    }
}
