﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MutantMenu : MonoBehaviour {
    public Animator anim;
    private SoundPlayer sound;
    // Use this for initialization
    void Start()
    {
        anim = GetComponentInChildren<Animator>();
        sound = GetComponent<SoundPlayer>();
    }

    // Update is called once per frame
    public void Call () {
        
            anim.SetTrigger("On");
            sound.Play(0, 1);
       
    }
}
