﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StepSc : MonoBehaviour {
    public CharacterController cont;
    public SoundPlayer sound;
    bool sounding=false;
    bool metal = true;
    private void Start()
    {
        sound = gameObject.GetComponent<SoundPlayer>();
    }
    private void Update()
    {
        //Debug.Log(metal);
        if (cont.isGrounded && !sounding && metal && (Input.GetAxis("Vertical") != 0||Input.GetAxis("Horizontal")!=0))
        {
            sounding = true;
            StartCoroutine(soundHere());
            sound.Play(Random.Range(3, 6), 0.1F);
        }
        else if (cont.isGrounded && !sounding && (Input.GetAxis("Vertical") != 0 || Input.GetAxis("Horizontal") != 0))
        {
            sounding = true;
            StartCoroutine(soundHere());
            sound.Play(Random.Range(0, 2), 0.1F);
        }
    }
    private void LateUpdate()
    {
        //metal = false;
    }
    private void OnCollisionEnter(Collision collision)
    {
        //Debug.Log(cont.isGrounded);
        //if (collision.gameObject.tag == "Terrain" && !sounding&&cont.isGrounded)
        //{
            
        //}
        if (collision.gameObject.tag == "Metal")
        {
            metal = true;
        }
        else
        {
            metal = false;
        }
        

    }
    IEnumerator soundHere()
    {
        yield return new WaitForSeconds(0.5F);
        sounding = false;
    }
}
