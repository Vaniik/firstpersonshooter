﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager2 : MonoBehaviour {
    public Image img;
    public MutantMenu MM;
    public SoldierMenu SM;
    public Button starting;
    public AudioSource Sound;
    public SoundPlayer Sound2;
    private void Start()
    {
        Sound = gameObject.GetComponent<AudioSource>();
        Sound2 = gameObject.GetComponent<SoundPlayer>();
        //Cursor.lockState = CursorLockMode.None;
        //Debug.Log(Cursor.lockState);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow)||Input.GetKeyDown(KeyCode.UpArrow ))
        {
            Sound2.Play(0, 1);
        }
        //Debug.Log(Cursor.lockState);
    }
    public void EmpiezaJuego(){
        StartCoroutine(FadeImage(false));
        Sound.Stop();
        MM.Call();
        SM.Call();
        Debug.Log("Entro");
        Cursor.lockState = CursorLockMode.Locked;
    }
    public void Exit()
    {
        Application.Quit();
    }
    IEnumerator FadeImage(bool fadeAway)
    {
        // fade from opaque to transparent
        if (fadeAway)
        {
            // loop over 1 second backwards
            for (float i = 3; i >= 0; i -= Time.deltaTime)
            {
                // set color with i as alpha
                img.color = new Color(0, 0, 0, i/3);
                yield return null;
            }
        }
        // fade from transparent to opaque
        else
        {
            // loop over 1 second
            for (float i = 0; i <= 2; i += Time.deltaTime)
            {
                // set color with i as alpha
                img.color = new Color(0, 0, 0, i/2);
                yield return null;
            }
        }
        SceneManager.LoadScene(1);
    }
}
