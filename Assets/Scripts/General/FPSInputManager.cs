﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSInputManager : MonoBehaviour {

	private PlayerMovement playerController;
	private float sensitivity = 3.0f;
	private LookRotation lookRotation;
	private MouseCursor mouseCursor;
    private bool dontBug=false;
	private Laser laser;
	private BallShoot ballShoot;
    private RocketShoot rocketShoot;
    public Image img;


    void Start ()
	{
		playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
		lookRotation = playerController.GetComponent<LookRotation>();
		laser = playerController.GetComponent<Laser> ();
		ballShoot = playerController.GetComponent<BallShoot> ();
        rocketShoot = playerController.GetComponent<RocketShoot>();

        mouseCursor = new MouseCursor();
		mouseCursor.HideCursor();
        StartCoroutine(FadeImage(true));
	}

	void Update ()
	{
		//El movimiento del player
		Vector2 inputAxis = Vector2.zero;
		inputAxis.x = Input.GetAxis("Horizontal");
		inputAxis.y = Input.GetAxis("Vertical");
		playerController.SetAxis(inputAxis);
		//El salto del player
		if(Input.GetButton("Jump")) playerController.StartJump();

		//Rotación de la cámara
		Vector2 mouseAxis = Vector2.zero;
		mouseAxis.x = Input.GetAxis("Mouse X") * sensitivity;
		mouseAxis.y = Input.GetAxis("Mouse Y") * sensitivity;
		//Debug.Log("Mouse X = " + mouseAxis.x + " Y = " + mouseAxis.y);
		lookRotation.SetRotation(mouseAxis);

		//Cursor del ratón
		if(Input.GetMouseButtonDown(0)) mouseCursor.HideCursor();
		else if(Input.GetKeyDown(KeyCode.Escape)) mouseCursor.ShowCursor();

        //if(Input.GetMouseButtonDown(0)) laser.Shot();
        //if(Input.GetKeyDown(KeyCode.R)) laser.Reload();
        //recargar
        if (Input.GetKey(KeyCode.LeftShift))
        {
            playerController.speed = 8;
            playerController.jumpSpeed = 9;
        }
        else{
            playerController.speed = 5;
            playerController.jumpSpeed = 8;
        }

        if (Input.GetKeyDown(KeyCode.R) && !ballShoot.reloadState())
        {
            StartCoroutine(ballShoot.StartReload());
            StartCoroutine(rocketShoot.StartReload());
        }
        //Disparos
        if (Input.GetMouseButton(0))
        {
            if (ballShoot.getAmmo() > 0) {
                
                if (!ballShoot.firingState()&&!ballShoot.reloadState())
                    StartCoroutine(ballShoot.StartShot());
            }
            else if(!ballShoot.reloadState()&&ballShoot.getMaxAmmo()>0)
            {
                StartCoroutine(ballShoot.StartReload());
            }
            else if(!ballShoot.reloadState()){
                //recargando
            }
            else{
                if (!dontBug)
                {
                    GameObject.FindGameObjectWithTag("Player").GetComponent<SoundPlayer>().Play(2, 1);
                    StartCoroutine(dont());
                }
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            if (rocketShoot.getAmmo() > 0)
            {
                if (!rocketShoot.firingState() && !ballShoot.reloadState())
                    StartCoroutine(rocketShoot.StartShot());
            }
            else if (!rocketShoot.reloadState() && rocketShoot.getMaxAmmo() > 0)
            {
                StartCoroutine(rocketShoot.StartReload());
            }
            else if (!rocketShoot.reloadState())
            {
                //recargando
            }
            else
            {
                //sonido de no municion
            }
        }
        //rocketShoot.Shot();

        

    }
    IEnumerator FadeImage(bool fadeAway)
    {
        // fade from opaque to transparent
        if (fadeAway)
        {
            
            yield return new WaitForSeconds(1F);
            // loop over 1 second backwards
            for (float i = 2; i >= 0; i -= Time.deltaTime)
            {
                // set color with i as alpha
                img.color = new Color(0, 0, 0, i / 2);
                yield return null;
            }
        }
        // fade from transparent to opaque
        else
        {
            // loop over 1 second
            for (float i = 0; i <= 2; i += Time.deltaTime)
            {
                // set color with i as alpha
                img.color = new Color(0, 0, 0, i / 2);
                yield return null;
            }
        }
        
    }
    IEnumerator dont()
    {
        dontBug = true;
        yield return new WaitForSeconds(0.1F);
        dontBug = false;

    }
}
