﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SoldierBehaviour : MonoBehaviour
{
    public enum State { Idle, Patrol, Chase, Shot, Dead};
    public State state;

    private NavMeshAgent agent;
    private Animator anim;
    private SoundPlayer sound;
   

    [Header("Creeper properties")]
    public int life = 5;

    [Header("Target Detection")]
    public float radius;
    public float idleRadius;
    public float chaseRadius;
    public LayerMask targetMask;
    public bool targetDetected = false;
    private Transform targetTransform;
    public bool TargetOnBox=false;

    [Header("Patrol path")]
    public bool stopAtEachNode = true;
    public float timeStopped = 1.0f;
    private float timeCounter = 0.0f;
    public Transform[] pathNodes;
    private int currentNode = 0;
    private bool nearNode = false;
    /*
    [Header("Explosion properties")]
    public float explodeDistance;
    public float explosionRadius;
    public float explosionForce;
    public ParticleSystem explosionPS;
    */
    // Use this for initialization
    [Header("Attack properties")]
    public GameObject bullet_prefab;
    public GameObject weapon;
    public float bulletImpulse = 20f;
    public float fireRate = 0F;
    private bool isShooting = false;
    public float attackDistance = 0F;

    private float SoundVolume;

    void Start ()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
        sound = GetComponentInChildren<SoundPlayer>();
        

        nearNode = true;
        SetIdle();        
	}
	
	// Update is called once per frame
	void Update ()
    {
        float dist = Vector3.Distance(gameObject.transform.position, GameObject.FindGameObjectWithTag("Player").transform.position);
        SoundVolume = 2F - dist / 10;
        if (SoundVolume < 0)
        {
            SoundVolume = 0;
        }
        if (state != State.Dead)
        {
            switch (state)
            {
                case State.Idle:
                    Idle();
                    break;
                case State.Patrol:
                    Patrol();
                    break;
                case State.Chase:
                    Chase();
                    break;

                case State.Dead:
                    Dead();
                    break;
                default:
                    break;
            }
        }
    }
    private void FixedUpdate()
    {
        targetDetected = false;
        Collider[] cols = Physics.OverlapSphere(this.transform.position, radius, targetMask);
        if(cols.Length != 0)
        {
            targetDetected = true;
            targetTransform = cols[0].transform;            
        }
    }

    void Idle()
    {
        if(targetDetected)
        {
            SetChase();
            return;
        }

        if(timeCounter >= timeStopped)
        {
            if(nearNode) GoNearNode();
            else GoNextNode();
            SetPatrol();
        }
        else timeCounter += Time.deltaTime;
    }
    void Patrol()
    {
        if(targetDetected)
        {
            SetChase();
            return;
        }

        if(agent.remainingDistance <= 0.1f)
        {
            if(stopAtEachNode) SetIdle();
            else GoNextNode();
        }
    }
    void Chase()
    {
        if(!targetDetected)
        {
            nearNode = true;
            SetIdle();
            
            return;
        }

        agent.SetDestination(targetTransform.position);
        if (Vector3.Distance(transform.position, targetTransform.position) <= attackDistance){
            agent.velocity=new Vector3(0,0,0);
            if (!isShooting) {
                anim.SetTrigger("InAim");
                anim.SetBool("Shooting", true);
                anim.SetBool("AlreadyShooting", true);
                StartCoroutine(StartShot());
            }
            else
            {
                
            }    
        }
        else
        {
            anim.SetBool("Shooting", false);
            anim.SetBool("AlreadyShooting", false);
            anim.SetTrigger("InChase");
        }
        
        
        
        


    }
    public void Shot()
    {
        sound.Play(0, SoundVolume);
        GameObject thebullet = (GameObject)Instantiate(bullet_prefab, weapon.transform.position + weapon.transform.forward, weapon.transform.rotation);
        thebullet.GetComponent<Rigidbody>().AddForce(weapon.transform.forward * bulletImpulse, ForceMode.Impulse);
        
    }
    public IEnumerator StartShot()
    {
        
        //yield return new WaitForSeconds(0.82F);
        isShooting = true;
        yield return new WaitForSeconds(0.5F);
        if (TargetOnBox)
        {
            Shot();
        }
        yield return new WaitForSeconds(fireRate);
        isShooting = false;

    }
    void Explode() { }
    void Dead() { }

    void SetIdle()
    {
        agent.isStopped = true;
        anim.SetTrigger("InIdle");
        anim.SetBool("isMoving", false);
        radius = idleRadius;
        timeCounter = 0;


        StartCoroutine(communicate());
        anim.SetTrigger("StopChasing");
        state = State.Idle;
    }
    void SetPatrol()
    { 
        agent.isStopped = false;
        agent.stoppingDistance = 0;
        radius = idleRadius;
        anim.SetTrigger("OutIdle");
        
        anim.SetBool("isMoving", true);
        anim.SetTrigger("StopChasing");
        state = State.Patrol;
    }
    void SetChase()
    {
        StartCoroutine(communicate());
        agent.isStopped = false;
        agent.SetDestination(targetTransform.position);
        agent.stoppingDistance = 2.0f;
        anim.SetBool("AlreadyShooting", false);
        radius = chaseRadius;

        state = State.Chase;
    }
    
    void SetDead()
    {
        sound.Play(Random.Range(1,2), SoundVolume);
        gameObject.GetComponent<BoxCollider>().enabled = false;
        agent.isStopped = true;
        anim.SetTrigger("InDie");
        StartCoroutine(Disolve());
        state = State.Dead;
    }
    void SetAttack()
    {
        anim.SetBool("AlreadyShooting", true);
    }

    void GoNextNode()
    {
        currentNode++;
        if(currentNode >= pathNodes.Length) currentNode = 0;

        agent.SetDestination(pathNodes[currentNode].position);
    }
    void GoNearNode()
    {
        nearNode = false;
        float minDistance = Mathf.Infinity;
        for(int i = 0; i < pathNodes.Length; i++)
        {
            if(Vector3.Distance(transform.position, pathNodes[i].position) < minDistance)
            {
                minDistance = Vector3.Distance(transform.position, pathNodes[i].position);
                currentNode = i;
            }
        }
        agent.SetDestination(pathNodes[currentNode].position);
    }

    
    private void OnDrawGizmos()
    {
        Color color = Color.green;
        if(targetDetected) color = Color.red;
        color.a = 0.1f;
        Gizmos.color = color;
        Gizmos.DrawSphere(this.transform.position, radius);
    }

    public void Damage(int hit)
    {
        Debug.Log("Creeper damage");
        if(state == State.Dead) return;
        life -= hit;
        if(life <= 0) SetDead();
    }

	void OnCollisionEnter(Collision other) {

		if(other.gameObject.tag == "Shot") {
			Damage (5);	
		}
	}
    private IEnumerator Disolve()
    {

        yield return new WaitForSeconds(8F);
        
        
        Destroy(this.gameObject);
    }
    public IEnumerator communicate()
    {
        sound.Play(Random.Range(3, 6), SoundVolume);
        yield return new WaitForSeconds(0.6F);
        sound.Play(Random.Range(3, 6), SoundVolume);
        yield return new WaitForSeconds(0.6F);
        sound.Play(Random.Range(3, 6), SoundVolume);
    }
}
