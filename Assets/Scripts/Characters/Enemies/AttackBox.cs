﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackBox : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            BallShoot Player= other.gameObject.GetComponent<BallShoot>();
            Player.damaged(15F);
        }
    }
    // Use this for initialization
}