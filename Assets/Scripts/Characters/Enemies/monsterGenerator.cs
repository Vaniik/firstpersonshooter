﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class monsterGenerator : MonoBehaviour {

	public GameObject monster;
	public Transform[] points;
    [Header("Marca solo uno o la lias parda")]
    public bool Spider;
    public bool Soldier;
	public float timeToSpawn;

	IEnumerator Start(){
		while (true) {
			yield return new WaitForSeconds (timeToSpawn);

			GameObject imonster = Instantiate (monster,transform.position,transform.rotation);
            
            if (Spider)
            {
                SpiderBehaviour sb = imonster.GetComponent<SpiderBehaviour>();
                sb.pathNodes = points;
            }
            else if (Soldier)
            {
                SoldierBehaviour sb = imonster.GetComponent<SoldierBehaviour>();
                sb.pathNodes = points;
            }
			
		}
	}
}
