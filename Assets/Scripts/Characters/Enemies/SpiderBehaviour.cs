﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpiderBehaviour : MonoBehaviour
{
    public enum State { Idle, Patrol, Chase, Explode, Dead};
    public State state;

    private NavMeshAgent agent;
    public Animator anim;
    private SoundPlayer sound;
    private float SoundVolume=0;
    private Transform player;
    [Header("Creeper properties")]
    public int life = 5;

    [Header("Target Detection")]
    public float radius;
    public float idleRadius;
    public float chaseRadius;
    public LayerMask targetMask;
    public bool targetDetected = false;
    private Transform targetTransform;

    [Header("Patrol path")]
    public bool stopAtEachNode = true;
    public float timeStopped = 1.0f;
    private float timeCounter = 0.0f;
    public Transform[] pathNodes;
    private int currentNode = 0;
    private bool nearNode = false;
    /*
    [Header("Explosion properties")]
    public float explodeDistance;
    public float explosionRadius;
    public float explosionForce;
    public ParticleSystem explosionPS;
    */
    // Use this for initialization
    [Header("Attack properties")]
    public float SpiderFirerate=0F;
    private bool isAttacking;
    public GameObject attackingCollider;
    public float attackDistance;
    void Start ()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();
        sound = GetComponentInChildren<SoundPlayer>();
        player = GameObject.FindGameObjectWithTag("Player").transform;

        nearNode = true;
        SetIdle();        
	}
	
	// Update is called once per frame
	void Update ()
    {
        float dist = Vector3.Distance(gameObject.transform.position, player.transform.position);
        SoundVolume = 4F - dist/3;
        if (SoundVolume < 0)
        {
            SoundVolume = 0;
        }
        if (state != State.Dead)
        {
            switch (state)
            {
                case State.Idle:
                    Idle();
                    break;
                case State.Patrol:
                    Patrol();
                    break;
                case State.Chase:
                    Chase();
                    break;
                case State.Explode:
                    Attack();
                    break;
                case State.Dead:
                    Dead();
                    break;
                default:
                    break;
            }
        }
    }
    private void FixedUpdate()
    {
        targetDetected = false;
        Collider[] cols = Physics.OverlapSphere(this.transform.position, radius, targetMask);
        if(cols.Length != 0)
        {
            targetDetected = true;
            targetTransform = cols[0].transform;            
        }
    }

    void Idle()
    {
        if(targetDetected)
        {
            SetChase();
            return;
        }

        if(timeCounter >= timeStopped)
        {
            if(nearNode) GoNearNode();
            else GoNextNode();
            SetPatrol();
        }
        else timeCounter += Time.deltaTime;
    }
    void Patrol()
    {
        if(targetDetected)
        {
            SetChase();
            return;
        }

        if(agent.remainingDistance <= 0.1f)
        {
            if(stopAtEachNode) SetIdle();
            else GoNextNode();
        }
    }
    void Chase()
    {
        if(!targetDetected)
        {
            nearNode = true;
            SetIdle();
            
            return;
        }

        agent.SetDestination(targetTransform.position);
        if (Vector3.Distance(transform.position, targetTransform.position) <= attackDistance)
        {
            Attack();
        }
        


    }
    void Attack()
    {
        if (!isAttacking)
        {
            sound.Play(0,SoundVolume);
            StartCoroutine(DoAttack());
            SetAttack();
        }
    }
    IEnumerator DoAttack()
    {
        isAttacking = true;
        attackingCollider.SetActive(true);
        yield return new WaitForSeconds(0.5F);
        attackingCollider.SetActive(false);
        if(state!=State.Dead)
            SetChase();
        StartCoroutine(waitFireRate());
    }
    IEnumerator waitFireRate()
    {
        yield return new WaitForSeconds(SpiderFirerate);
        isAttacking = false;
    }
    void Explode() { }
    void Dead() { }

    void SetIdle()
    {
        agent.isStopped = true;        
        anim.SetTrigger("Idle");
        radius = idleRadius;
        timeCounter = 0;
        int rand = Random.Range(1, 2);
        sound.Play(rand, SoundVolume);


        state = State.Idle;
    }
    void SetPatrol()
    { 
        agent.isStopped = false;
        agent.stoppingDistance = 0;
        radius = idleRadius;
        anim.SetTrigger("Walk");

        state = State.Patrol;
    }
    void SetAttack()
    {
        
            
        
        agent.isStopped = true;
        agent.stoppingDistance = 0;
        //radius = idleRadius;
        anim.SetTrigger("Attack");

        state = State.Explode;
    }
    void SetChase()
    {
        
            int rand = Random.Range(1,2);
            sound.Play(rand, SoundVolume);
        
        
        agent.isStopped = false;
        agent.SetDestination(targetTransform.position);
        agent.stoppingDistance = 2.0f;
        anim.SetTrigger("Run");
        radius = chaseRadius;

        state = State.Chase;
    }
    
    void SetDead()
    {
        sound.Play(3, SoundVolume/3);
        agent.isStopped = true;
        anim.SetTrigger("Die");
        gameObject.GetComponent<BoxCollider>().enabled = false;
        
        state = State.Dead;
        StartCoroutine(Disolve());
    }

    void GoNextNode()
    {
        currentNode++;
        if(currentNode >= pathNodes.Length) currentNode = 0;

        agent.SetDestination(pathNodes[currentNode].position);
    }
    void GoNearNode()
    {
        nearNode = false;
        float minDistance = Mathf.Infinity;
        for(int i = 0; i < pathNodes.Length; i++)
        {
            if(Vector3.Distance(transform.position, pathNodes[i].position) < minDistance)
            {
                minDistance = Vector3.Distance(transform.position, pathNodes[i].position);
                currentNode = i;
            }
        }
        agent.SetDestination(pathNodes[currentNode].position);
    }

    
    private void OnDrawGizmos()
    {
        Color color = Color.green;
        if(targetDetected) color = Color.red;
        color.a = 0.1f;
        Gizmos.color = color;
        Gizmos.DrawSphere(this.transform.position, radius);
    }

    public void Damage(int hit)
    {
        Debug.Log("Creeper damage");
        if(state == State.Dead) return;
        life -= hit;
        if(life <= 0) SetDead();
    }

	void OnCollisionEnter(Collision other) {

		if(other.gameObject.tag == "Shot") {
			Damage (5);	
		}
	}
    private IEnumerator Disolve()
    {
        //SetDead();
        yield return new WaitForSeconds(8F);


        Destroy(this.gameObject);
    }
}
