using UnityEngine;
using System.Collections;

public class RocketShoot : MonoBehaviour {
	
	public GameObject rocket_prefab;
    public GameObject plasmaExplosion;
    public float bulletImpulse = 20f;
    public float reloadTime = 0F;
    public float fireRate = 0F;
    public int maxAmmo = 30;
    public int cargadores = 2;

    private int CurrAmmo = 0;
    private bool isReloading = false;
    private bool isShooting = false;
    private void Start()
    {
        CurrAmmo = maxAmmo;
    }


    // Update is called once per frame
    public void Shot()
    {
        gameObject.GetComponent<SoundPlayer>().Play(4, 1);
        GameObject thebullet = (GameObject)Instantiate(rocket_prefab, Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.rotation);
        thebullet.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * bulletImpulse, ForceMode.Impulse);
        CurrAmmo -= 1;
    }

    public int getAmmo()
    {
        return CurrAmmo;
    }
    public int getMaxAmmo()
    {
        int tempMax = maxAmmo * cargadores;
        return tempMax;
    }
    public IEnumerator StartReload()
    {
        isReloading = true;
        yield return new WaitForSeconds(reloadTime);

        CurrAmmo = maxAmmo;
        isReloading = false;

    }
    public IEnumerator StartShot()
    {
        isShooting = true;
        Shot();
        yield return new WaitForSeconds(fireRate);
        isShooting = false;

    }
    public bool reloadState()
    {
        return isReloading;
    }
    public bool firingState()
    {
        return isShooting;
    }
}
