﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoSecSc : MonoBehaviour {
    public GameObject player;
    public GameObject[] comp;
    public BoxCollider box;
    public SoundPlayer sound;
    public float timeRe;
    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        sound = gameObject.GetComponent<SoundPlayer>();
        //comp = gameObject.GetComponentsInChildren<GameObject>();
       // box = gameObject.GetComponent<BoxCollider>();
    }
    private void Update()
    {
        gameObject.transform.Rotate(new Vector3(0, 1F, 0));
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other = player.GetComponent<Collider>())
        {
            player.GetComponent<RocketShoot>().cargadores += 1;
            foreach (GameObject go in comp)
            {
                go.SetActive(false);
            }
            sound.Play(0, 1);
            box.enabled = false;
            StartCoroutine(recharge());
        }
    }
    IEnumerator recharge()
    {

        yield return new WaitForSeconds(timeRe);
        foreach (GameObject go in comp)
        {
            go.SetActive(true);
        }
        box.enabled = true;
    }


}
