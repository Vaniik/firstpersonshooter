using UnityEngine;
using System.Collections;

public class BallDetonatorEnem : MonoBehaviour {
	
	float lifespan = 3.0f;
	public GameObject fireEffect;
	
	// Update is called once per frame
	void Update () {
		lifespan -= Time.deltaTime;
		
		if(lifespan <= 0) {
			//Explode();
		}
	}
	
	void OnCollisionEnter(Collision collision) {
       
            if (collision.gameObject.tag == "Player")
            {
                //collision.gameObject.tag = "Untagged";
                BallShoot Player = collision.gameObject.GetComponent<BallShoot>();
                Player.damaged(25F);

            }
            Explode();
        

    }
	
	void Explode() {
		
		Destroy(gameObject);
	}
}
