using UnityEngine;
using System.Collections;

public class BallDetonator : MonoBehaviour {
	
	float lifespan = 3.0f;
	public GameObject fireEffect;
    public GameObject fireEffect2;
  
    public GameObject death;

    // Update is called once per frame
    void Update () {
		lifespan -= Time.deltaTime;
		
		if(lifespan <= 0) {
			Explode();
		}
	}
	
	void OnCollisionEnter(Collision collision) {

        //if(collision.gameObject.tag == "Enemy") {
        //collision.gameObject.tag = "Untagged";
        //GameObject fire = Instantiate(fireEffect, collision.contacts[0].point, new Quaternion(collision.contacts[0].normal.x, collision.contacts[0].normal.y, collision.contacts[0].normal.z, 100));
        //GameObject fire2 = Instantiate(fireEffect2, collision.contacts[0].point, new Quaternion(collision.contacts[0].normal.x, collision.contacts[0].normal.y, collision.contacts[0].normal.z, 100));
        //fire.AddComponent<RemoveExplosion> ();
        GameObject fire = Instantiate(fireEffect, collision.contacts[0].point, Quaternion.FromToRotation(Vector3.forward, collision.contacts[0].normal));
        GameObject fire2 = Instantiate(fireEffect2, collision.contacts[0].point, Quaternion.FromToRotation(Vector3.forward, collision.contacts[0].normal));
        if (collision.gameObject.tag != "Enemy")
        {
            GameObject fire3 = Instantiate(death, collision.contacts[0].point, Quaternion.FromToRotation(Vector3.forward, collision.contacts[0].normal));
        }

        Explode();		
		//}
        
    }
	
	void Explode() {

        Destroy(gameObject);
    }
 
}
