using UnityEngine;
using System.Collections;

public class RocketDetonator : MonoBehaviour {

	float lifespan = 2.0f;

	public GameObject fireEffect;
    public GameObject plasmaExplosion;
    public GameObject[] portals;
    public PortalSystem PS;
    private void Start()
    {
        portals = GameObject.FindGameObjectsWithTag("Portal");
        PS = GameObject.FindGameObjectWithTag("PortalSystem").GetComponent<PortalSystem>();
        StartCoroutine(Explode());
    }
    // Update is called once per frame
   
	
	void OnCollisionEnter(Collision collision) {
		
		
			//collision.gameObject.tag = "Untagged";
			//GameObject fire = Instantiate(fireEffect, gameObject.transform.position, Quaternion.identity);
			//fire.AddComponent<RemoveExplosion> ();
			//Explode();		
	
	}
	
	IEnumerator Explode() {
        yield return new WaitForSeconds(lifespan);
        if (PS.lastPortal == 0)
        {
            portals[0].transform.position = new Vector3(this.transform.position.x,this.transform.position.y+1,this.transform.position.z);
            portals[0].GetComponent<Portal>().disabled = false;
            PS.lastPortal = 1;
        }
        else
        {
            portals[1].transform.position = new Vector3(this.transform.position.x, this.transform.position.y + 1, this.transform.position.z);
            portals[1].GetComponent<Portal>().disabled = false;
            PS.lastPortal = 0;
        }
        GameObject plasma = (GameObject)Instantiate(plasmaExplosion,gameObject.transform.position,Quaternion.identity);
        Destroy(gameObject);
	}
}
