﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class MaxAmmoSecHUD_Sc : MonoBehaviour {
    public RocketShoot BS;
    private TextMeshProUGUI TMPRO;
	// Use this for initialization
	void Start () {
        TMPRO = gameObject.GetComponent<TextMeshProUGUI>();
	}
	
	// Update is called once per frame
	void Update () {
        TMPRO.SetText(BS.getMaxAmmo().ToString());
	}
}
