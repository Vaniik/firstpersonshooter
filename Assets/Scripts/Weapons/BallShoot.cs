using UnityEngine;
using System.Collections;

public class BallShoot : MonoBehaviour {
	[Header("Arma primaria")]
	public GameObject bullet_prefab;
    public ParticleSystem Fire;
    public Transform WeaponCanon;
	public float bulletImpulse = 20f;
    public float reloadTime = 0F;
    public float fireRate = 0F;
    public int maxAmmo = 30;
    public int cargadores = 2;

    private int CurrAmmo = 0;
    private bool isReloading = false;
    private bool isShooting = false;
    private void Start()
    {
        CurrAmmo = maxAmmo;
        currHP = maxHP;
        currShield = maxShield;
    }
    [Header("vida")]
    public float maxHP;
    private float currHP;
    public float maxShield;
    private float currShield;
    public float restoreShieldTime;
    public bool hitted=false;
    public float invencibleTime;
    // Update is called once per frame
    
    public void Shot () {
        gameObject.GetComponent<SoundPlayer>().Play(Random.Range(0,1), 1);
		GameObject thebullet = (GameObject)Instantiate(bullet_prefab, WeaponCanon.position + WeaponCanon.forward, WeaponCanon.rotation);
		thebullet.GetComponent<Rigidbody>().AddForce( Camera.main.transform.forward * bulletImpulse, ForceMode.Impulse);
        CurrAmmo -= 1;
	}

    public int getAmmo()
    {
        return CurrAmmo;
    }
    public int getMaxAmmo()
    {
        int tempMax = maxAmmo * cargadores;
        return tempMax;
    }
    public IEnumerator StartReload()
    {
        isReloading = true;
        gameObject.GetComponent<SoundPlayer>().Play(3, 1);
        yield return new WaitForSeconds(reloadTime);

        CurrAmmo = maxAmmo;
        isReloading = false;

    }
    public IEnumerator StartShot()
    {
        isShooting = true;
        Shot();
        Fire.Play();
        yield return new WaitForSeconds(fireRate);
        //Fire.Stop();
        isShooting = false;
        
    }
    public bool reloadState()
    {
        return isReloading;
    }
    public bool firingState()
    {
        return isShooting;
    }
    public float getHP() { return currHP; }
    public float getShield() { return currShield; }
    public void damaged(float dmg)
    {
        
        //StopCoroutine(invulnerable());
        if (currShield <= 0)
        {
            currHP -= dmg;
        }
        else
        {
            currShield -= dmg;
            if (currShield < 0)
            {
                currShield = 0;
            }
        }
        //StartCoroutine(invulnerable());
    }
    private IEnumerator invulnerable()
    {
        hitted = true;
        yield return new WaitForSeconds(invencibleTime);
        hitted = false;
        StartCoroutine(restoreShield());
    }
    private IEnumerator restoreShield()
    {
        yield return new WaitForSeconds(restoreShieldTime);
        bool restore = true;
        while (restore)
        {
            if (!hitted)
            {
                if (currShield <= maxShield)
                {
                    currShield += 0.5F;
                }
                else
                {
                    restore = false;
                }

            }
            else
            {
                restore = false;
                
                Debug.Log("Hitted");
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
