﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AmmoHUD_Sc : MonoBehaviour {

    public BallShoot BS;
    private TextMeshProUGUI TMPRO;
    // Use this for initialization
    void Start()
    {
        TMPRO = gameObject.GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        
        TMPRO.SetText(BS.getAmmo().ToString());
        if (BS.reloadState())
        {
            TMPRO.color = new Color32(180, 30, 30, 255);
        }
        else
        {
            TMPRO.color = new Color32(255, 255, 255, 255);
        }
    }
}
