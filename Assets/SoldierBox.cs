﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierBox : MonoBehaviour {
    public SoldierBehaviour SB;
    // Use this for initialization
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            SB.TargetOnBox = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            SB.TargetOnBox = false;
        }
    }
}
