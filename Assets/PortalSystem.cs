﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalSystem : MonoBehaviour {
    public GameObject player;
    public int lastPortal;
    private void Start()
    {
        player=GameObject.FindGameObjectWithTag("Player");
    }
    public void UsePortal(Portal Used,Portal Destined)
    {
        player.transform.position = Destined.transform.position;
        player.transform.Rotate(new Vector3(player.transform.rotation.x, player.transform.rotation.y+180, player.transform.rotation.z));
        //Destined.disabled = true;
        //Used.disabled = true;
        StartCoroutine( Used.enable());
        StartCoroutine(Destined.enable());
    }
}
